#include "stdafx.h"
#include <iostream>
#include <cmath>


void InverseKinematics(double position[3]);


int main()
{
	bool loop = true;
	int status = 1;
	while (loop == true)
	{
		std::cout << "1 to set values, 0 to quit";
		std::cin >> status;
		if (status == 1)
		{
			double test[3] = { 1,1,1 };
			std::cout << "enter x,y,z\n";
			std::cin >> test[0] >> test[1] >> test[2];
			InverseKinematics(test);
		}
		else if (status != 1)
		{
			loop = false;
		}

	}
	return 0;
}

void InverseKinematics(double position[3])
{


	//INVERSE SOLUTION
	double px, py, pz;
	pz = position[2];
	py = position[1];
	px = position[0];


	double O[4][4] = {
		{ 1,0,0.1,0 },
		{ 0,1,0.1,0 },
		{ 0,0,1,0 },
		{ 0,0,0,1 }
	};
	double theta1 = 1, theta2 = 1, theta3 = 1, theta4 = 1, theta5 = 1, theta234 = 1;
	double cosine3, sine3;

	double cross2 = 0.1016;
	double cross3 = 0.088;
	double cross4 = 0.056;

	theta1 = atan(py / px);
	theta234 = atan(O[3][3] / (cos(theta1)*O[3][1] + sin(theta1)*O[3][2]));
	cosine3 = (pow((px*cos(theta1) + py*sin(theta1) - cos(theta234)*cross4), 2) + pow((pz - sin(theta234)*cross4), 2) - pow(cross2, 2) - pow(cross4, 2)) / (2 * cross2*cross3);
	sine3 = sqrt(1 - pow(cos(theta3), 2));
	theta3 = atan(sin(theta3) / cos(theta3));
	theta2 = atan(((cos(theta3)*cross3 + cross2)*(pz - sin(theta234)*cross4) - ((sin(theta3)*cross3)*(px*cos(theta1) + py*sin(theta1) - cos(theta234)*cross4))) / ((cos(theta3)*cross3 + cross2)*(px*cos(theta1) + py*sin(theta1) - cos(theta234)*cross4)) + (sin(theta3)*cross3)*(pz - sin(theta234)*cross4));
	theta4 = theta234 - theta2 - theta3;
	theta5 = atan((cos(theta234)*(cos(theta1)*O[0][2] + sin(theta1)*O[1][2]) + sin(theta234)*O[0][3]) / ((sin(theta1)*O[0][2]) - cos(theta1)*O[1][2]));


	double thetadeg[5];
	thetadeg[0] = (theta1 * 60 / 3.14) + 90;
	thetadeg[1] = (theta2 * 60 / 3.14) + 90;
	thetadeg[2] = (theta3 * 60 / 3.14) + 90;
	thetadeg[3] = (theta4 * 60 / 3.14) + 90;
	thetadeg[4] = (theta5 * 60 / 3.14) + 90;

	for (int i = 0; i < 5; i++)
	{
	std::cout<<i<<":" <<thetadeg[i]<<std::endl;

	}


}
