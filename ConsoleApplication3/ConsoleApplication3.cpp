#include "stdafx.h"
#include <stdio.h>
#include <tchar.h>
#include "SerialClass.h"	// Library described above
#include <string>
#include <iostream>

void InverseKinematics(double position[3]); //Function Prototypes
int digits(int number);
double thetaArray[5];
void ArduinoLoop(void);

// application reads from the specified serial port and reports the collected data
int _tmain(int argc, _TCHAR* argv[])
{

	Serial* SP = new Serial("\\\\.\\COM6");    // adjust as needed

	if (SP->IsConnected())
		printf("We're connected");

	char incomingData[256] = "";			// don't forget to pre-allocate memory
											//printf("%s\n",incomingData);
	int dataLength = 255;
	int readResult = 0;
	int writeResult = 0;
	int angle1, angle2, angle3;
	int DELAY = 100;
	while (SP->IsConnected())
	{
		double oldpos[3] = { 1,1,1 };
		double pos[3] = { 1,1,1 };
		std::cin >> oldpos[0];
		std::cin >> oldpos[1];
		std::cin >> oldpos[2];
		InverseKinematics(oldpos);


		angle1 = (int)thetaArray[0];
		angle2 = (int)thetaArray[1];
		angle3 = (int)thetaArray[2];
		//int angle4 = (int)thetaArray[3];
		//int angle5 = (int)thetaArray[4];

		std::string s1 = std::to_string(angle1);
		const char *hold1 = s1.c_str();
		writeResult = SP->WriteData("1\n", 2);
		writeResult = SP->WriteData(hold1,digits(angle1));
		writeResult = SP->WriteData("\n",1);
		readResult = SP->ReadData(incomingData, dataLength);
		incomingData[readResult] = 0;
		printf("%s", incomingData);
		Sleep(DELAY);
		
		std::string s2 = std::to_string(angle2);
		const char *hold2 = s2.c_str();
		writeResult = SP->WriteData("2\n", 2);
		writeResult = SP->WriteData(hold2, digits(angle2));
		writeResult = SP->WriteData("\n", 1);
		readResult = SP->ReadData(incomingData, dataLength);
		incomingData[readResult] = 0;
		printf("%s", incomingData);
		Sleep(DELAY);
		
		std::string s3 = std::to_string(angle3);
		const char *hold3 = s3.c_str();
		writeResult = SP->WriteData("3\n", 2);
		writeResult = SP->WriteData(hold3, digits(angle3));
		writeResult = SP->WriteData("\n", 1);
		readResult = SP->ReadData(incomingData, dataLength);
		incomingData[readResult] = 0;
		printf("%s", incomingData);
		Sleep(DELAY);
	

	}
	return 0;
}
void ArduinoLoop(void)
{

}



void InverseKinematics(double position[3])
{



	//INVERSE SOLUTION
	double px, py, pz;
	pz = position[2];
	py = position[1];
	px = position[0];


	double O[4][4] = {
		{ 1,0,0,0 },
		{ 0,1,0,0 },
		{ 0,0,1,0 },
		{ 0,0,0,1 }
	};
	double theta1 = 1, theta2 = 1, theta3 = 1, theta4 = 1, theta5 = 1, theta234 = 1;
	double cosine3, sine3;
	double pi = 3.14159265;
	double cross2 = 0.1016;
	double cross3 = 0.0889;

	theta1 = atan2(py, px);
	cosine3 = (pow(px, 2) + pow(py, 2) + pow(pz, 2) - pow(cross2, 2) - pow(cross3, 2)) / (2 * cross2*cross3);
	//std::cout << "COS3: " << cosine3 << std::endl;
	//cosine3 = fmod(pi + cosine3, pi);
	//std::cout << "COS3 AFTER: " << cosine3 << std::endl;
	sine3 = sqrt(1 - pow(cos(theta3), 2));
	theta3 = atan2(sine3, cosine3);

	//theta3 = fmod(theta3 + pi, pi);
	theta2 = atan2(pz, sqrt(pow(pz, 2)) + pow(py, 2)) - atan2(cross3*sine3, cross2 + cross3*cosine3);
	
	std::cout << "Rad1: " << theta1<<"Rad2: "<<theta2<<"Rad3: "<<theta3 << std::endl;
	//theta1 = fmod(pi + theta1, pi);


	if (theta2 > 0)
	{
		theta2 =pi - theta2;
	}
	else if (theta2 < 0)
	{
		theta2 = pi - theta2;
	}

	//theta2 = fmod(pi + theta2, pi);

	double thetadeg[5];
	thetadeg[0] = (theta1 * 180 / pi);
	thetadeg[1] = (theta2 * 180 / pi);
	thetadeg[2] = (theta3 * 180 / pi);
	

	if (thetadeg[2] > 90)
	{
		thetadeg[2] = 180;
	}
	else if (thetadeg[2] < 90 && thetadeg[2] > 0)
	{

		thetadeg[2] = 135 + thetadeg[2];
	}
	else if (thetadeg[2] < 0 && thetadeg[2] > -135)
	{
		thetadeg[2] = 135 + thetadeg[2];
	}
	else if (thetadeg[2] < -135)
	{
		thetadeg[2] = 0;
	}


	

	

	
	for (int i = 0; i < 3; i++)
	{
		thetaArray[i] = thetadeg[i];
		std::cout << "ANGLE " << i + 1 << ":  " << thetaArray[i] << std::endl;

	}



//	std::cout << "theta1 =\t" << (theta1) << "\ntheta2 =\t" << (theta2) << "\ntheta3 =\t" << theta3 << std::endl;


}


int digits(int number)
{
	if (number / 100 >= 1)
		return 3;
	else if (number / 10 >= 1)
		return 2;
	else
		return 1;
}

